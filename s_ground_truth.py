# Copyright CNRS/Inria/UniCA
# Contributor(s): Eric Debreuve (since 2018)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import numpy as np
import os as os_
import skimage.color as cl_
import skimage.filters as fl_
import skimage.io as io_
import skimage.morphology as mp_
import skimage.segmentation as sgmt
import PyQt6.QtCore as qc_
import PyQt6.QtGui as qg_
import PyQt6.QtWidgets as qw_



circle_radius_g    = 3
magnifier_radius_g = 30

selection_on_g  = 100
selection_off_g = 255



class main_wdw_t(qw_.QMainWindow):

    observation = 0
    watershed   = 1


    def __init__(self, arguments):
        #
        super().__init__()
        self.setWindowTitle('S-Ground Truth')
        self.resize(600, 400)
        self.Center()

        layout = qw_.QGridLayout()

        main_wdw_t.__AddButtonToGridLayout__(layout, 0, 0, 'Choose Observation',  self.ChooseObservation)
        main_wdw_t.__AddButtonToGridLayout__(layout, 0, 1, 'Compute Superpixels', self.ComputeSuperpixels)
        main_wdw_t.__AddButtonToGridLayout__(layout, 2, 1, 'Save Ground Truth',   self.SaveGroundTruth)
        main_wdw_t.__AddButtonToGridLayout__(layout, 3, 1, 'Exit',                self.Exit)
        self.info_zone        = qw_.QLabel()
        self.static_info_zone = qw_.QLabel()
        self.static_info_zone.setAlignment(qc_.Qt.AlignmentFlag.AlignHCenter)
        layout.addWidget(self.info_zone,        2, 0)
        layout.addWidget(self.static_info_zone, 3, 0)

        main_wgt = qw_.QWidget()
        main_wgt.setLayout(layout)
        self.setCentralWidget(main_wgt)

        self.canvas = [canvas_t(self), canvas_t(self)]
        self.np_observation      = None
        self.np_superpixel_img   = None
        self.np_ground_truth_img = None
        self.gth_lbl_list        = [[]] # Could still contain empty lists when saving ground truth

        if arguments.__len__() > 1:
            self.ChooseObservation(False, arguments[1])
        else:
            self.ChooseObservation()

        layout.addWidget(self.canvas[self.observation], 1, 0)
        layout.addWidget(self.canvas[self.watershed],   1, 1)



    def ChooseObservation(self, _ = False, which = None):
        #
        if which is None:
            which, _ = qw_.QFileDialog.getOpenFileName(self, 'Choose Observation', qc_.QDir.currentPath())

        try:
            self.np_observation = io_.imread(which)
            self.canvas[self.observation].SetImage(self.np_observation)
            if self.np_observation.ndim == 3:
                self.np_observation = cl_.rgb2gray(self.np_observation)
            img_shape = self.np_observation.shape
            self.static_info_zone.setText(f'width={img_shape[1]}, height={img_shape[0]}')
        except Exception as error:
            if which.__len__() > 0:
                qw_.QMessageBox.critical(self, 'Error Loading Image',
                                         f'{os_.path.basename(which)}: Image cannot be loaded '
                                         f'from\n{os_.path.dirname(which)}.\n\nMessage:\n{error}')



    def ComputeSuperpixels(self):
        #
        if self.np_observation is None:
            return

        self.np_superpixel_img = WatershedImage(self.np_observation)
        self.InitializeGroundTruth()



    def InitializeGroundTruth(self):
        #
        self.np_ground_truth_img = InitialWatershedGroundTruth(self.np_superpixel_img)
        self.gth_lbl_list        = [[]] # Could still contain empty lists when saving ground truth

        obs_canvas = self.canvas[self.observation]
        spx_canvas = self.canvas[self.watershed]

        spx_canvas.SetImage(self.np_ground_truth_img)
        spx_canvas.ActivatePointerTracking(spx_canvas, obs_canvas, 255*self.np_observation)
        obs_canvas.ActivatePointerTracking(spx_canvas, spx_canvas, self.np_ground_truth_img)



    def SaveGroundTruth(self):
        """"""
        ground_truth = np.zeros(self.np_superpixel_img.shape, dtype = np.uint64)
        for idx, labels in enumerate(self.gth_lbl_list, start = 1):
            for label in labels:
                ground_truth[self.np_superpixel_img == label] = idx

        if ground_truth.max() == 0:
            return

        current_folder = qc_.QDir.currentPath()
        question = None
        while True:
            where, _ = qw_.QFileDialog.getSaveFileName(self, 'Choose Ground Truth Destination', current_folder,
                                                       options = qw_.QFileDialog.Option.DontConfirmOverwrite)
            if where.__len__() == 0:
                return

            if os_.path.exists(where):
                if question is None:
                    question = qw_.QMessageBox(self)
                    question.setWindowTitle('Warning: Existing Destination File')
                    question.setText(f'{os_.path.basename(where)}: Destination already exists '
                                     f'in\n{os_.path.dirname(where)}.\n\nAction to take:')
                    over_button   = question.addButton('Overwrite Destination',      qw_.QMessageBox.ButtonRole.YesRole)
                    change_button = question.addButton('Choose Another Destination', qw_.QMessageBox.ButtonRole.NoRole)
                    _             = question.addButton('Cancel Saving',              qw_.QMessageBox.ButtonRole.NoRole)
                    question.setDefaultButton(change_button)
                question.exec()

                clicked_button = question.clickedButton()
                if clicked_button == over_button:
                    break
                elif clicked_button == change_button:
                    current_folder = os_.path.dirname(where)
                else:
                    return
            else:
                break

        try:
            # import matplotlib.pyplot as plt
            # plt.matshow(ground_truth)
            # plt.show()
            io_.imsave(where, ground_truth)
        except Exception as error:
            qw_.QMessageBox.critical(self, 'Error Saving Image',
                                     f'{os_.path.basename(where)}: Ground Truth cannot be saved '
                                     f'in\n{os_.path.dirname(where)}.\n\nMessage:\n{error}')

            # import matplotlib.pyplot as plt
            # plt.matshow(ground_truth)
            # plt.show()
        # else:
        #     import matplotlib.pyplot as plt
        #     plt.matshow(self.np_superpixel_img)
        #     plt.show()
        #
        # print(self.gth_lbl_list)



    def Exit(self):
        #
        self.close()



    def Center(self):
        #
        screen_center = qg_.QScreen.availableGeometry(qw_.QApplication.primaryScreen()).center()
        geometry = self.frameGeometry()
        geometry.moveCenter(screen_center)
        self.move(geometry.topLeft())



    def keyPressEvent(self, event):
        #
        if event.key() == qc_.Qt.Key.Key_M:
            self.canvas[self.observation].ToggleMagnifier()
            self.canvas[self.watershed].ToggleMagnifier()



    @staticmethod
    def __AddButtonToGridLayout__(layout, row, col, title, action):
        #
        button = qw_.QPushButton(title)
        button.clicked.connect(action)
        layout.addWidget(button, row, col)



class simple_canvas_t(qw_.QLabel):

    def __init__(self):
        #
        super().__init__()

        self.setBackgroundRole(qg_.QPalette.ColorRole.Base)
        self.setSizePolicy(qw_.QSizePolicy.Policy.Ignored, qw_.QSizePolicy.Policy.Ignored)
        self.setScaledContents(True)
        self.setAttribute(qc_.Qt.WidgetAttribute.WA_TransparentForMouseEvents, True)

        self.qimage = None



    def SetImage(self, np_image):
        #
        # https://github.com/baoboa/pyqt5/blob/master/examples/widgets/imageviewer.py
        #
        self.qimage = simple_canvas_t.__QImageFromNPArray__(np_image) # Must be kept alive in object
        self.setPixmap(qg_.QPixmap.fromImage(self.qimage))



    @staticmethod
    def __QImageFromNPArray__(array):
        #
        if array.dtype.type is not np.uint32:
            array = array.astype(np.uint32)
        if array.ndim == 3:
            color_array = array
        else:
            color_array = np.dstack((array, array, array))

        rgba_img = 255 << 24 | color_array[:,:,0] << 16 | color_array[:,:,1] << 8 | color_array[:,:,2]

        return qg_.QImage(rgba_img.flatten(), array.shape[1], array.shape[0], qg_.QImage.Format.Format_RGB32)



class canvas_t(simple_canvas_t):

    def __init__(self, parent_obj):
        #
        super().__init__()
        self.parent_obj = parent_obj

        self.np_image   = None
        self.img_width  = None
        self.img_height = None

        self.magnifier_on = False
        self.mag_size  = 2 * magnifier_radius_g + 1
        self.magnifier = simple_canvas_t()
        self.magnifier.setParent(self)
        self.magnifier.setGeometry(0, 0, self.mag_size, self.mag_size)
        self.magnifier.SetImage(np.full((self.mag_size, self.mag_size), 255))
        self.magnifier.setVisible(False)
        center = circle_t(parent = self.magnifier)
        center.move(magnifier_radius_g - circle_radius_g, magnifier_radius_g - circle_radius_g)

        self.pointer = circle_t(parent = self)

        self.tgt_canvas    = None
        self.tgt_magnifier = None
        self.tgt_mag_img   = None
        self.tgt_pointer   = None



    def SetImage(self, np_image):
        #
        super().SetImage(np_image)
        self.np_image   = np_image
        self.img_width  = np_image.shape[1]
        self.img_height = np_image.shape[0]



    def ActivatePointerTracking(self, tgt_img_canvas, tgt_fdk_canvas, tgt_mag_image): # fdk=feedback
        #
        self.tgt_canvas    = tgt_img_canvas
        self.tgt_magnifier = tgt_fdk_canvas.magnifier
        self.tgt_mag_img   = tgt_mag_image
        self.tgt_pointer   = tgt_fdk_canvas.pointer
        self.setAttribute(qc_.Qt.WidgetAttribute.WA_TransparentForMouseEvents, False)
        self.setMouseTracking(True)
        #self.pointer.setVisible(True)



    def enterEvent(self, event: qc_.QEvent):
        #
        self.pointer.setVisible(False)
        self.tgt_pointer.setVisible(True)



    def leaveEvent(self, event: qc_.QEvent):
        #
        self.tgt_pointer.setVisible(False)



    def mousePressEvent(self, event):
        #
        pos_x, pos_y = self.__PixelCoordsFromEventXY__(event.position().x(), event.position().y())
        mods = event.modifiers()

        if mods == qc_.Qt.KeyboardModifier.NoModifier:
            mode = -1 # Superpixel selection or deselection mode
                      # In this mode, the target region is necessarily the latest one
        elif mods == qc_.Qt.KeyboardModifier.ShiftModifier:
            mode = 1 # Region completion mode
                     # In this mode, the target region is necessarily the latest one
        elif mods == qc_.Qt.KeyboardModifier.AltModifier:
            mode = 0 # Region deselection mode
                     # In this mode, the target region can be any region; It has to be identified. It also perform
                     # the completion of the current region implicitly.
        else:
            return

        spx_img = self.parent_obj.np_superpixel_img
        gth_img = self.parent_obj.np_ground_truth_img
        gth_lbl_list = self.parent_obj.gth_lbl_list
        selected_sp_label = spx_img[pos_y, pos_x]

        already_selected = False
        for labels in reversed(gth_lbl_list):
            if selected_sp_label in labels:
                already_selected = True
                break
        if already_selected:
            selection_value = selection_off_g
        else:
            selection_value = selection_on_g
        #     qw_.QMessageBox.warning(self, 'Superpixel Selection',
        #                             'Please click well inside superpixels (i.o.w, avoid frontiers).')
        #     return # A frontier has been clicked

        not_on_frontier = gth_img > 0
        if mode == -1:
            gth_img[(spx_img == selected_sp_label).__and__(not_on_frontier)] = selection_value
            if selection_value == selection_on_g:
                gth_lbl_list[-1].append(selected_sp_label)
            else:
                gth_lbl_list[-1].remove(selected_sp_label)
        elif mode == 1:
            if not already_selected:
                mask = np.zeros(gth_img.shape, dtype = np.uint8)
                for region_label in gth_lbl_list[-1]:
                    mask[spx_img == region_label] = 1
                labeled_mask, n_labels = mp_.label(1 - mask, connectivity = 1, return_num = True)
                if n_labels == 1:
                    qw_.QMessageBox.critical(self, 'Region Completion',
                                             'Please close region before requesting region filling and completion.')
                    return
                elif n_labels > 2:
                    qw_.QMessageBox.critical(self, 'Region Completion',
                                             'Region has multiple holes. Please fill all but the central one '
                                             'before requesting filling and completion.')
                    return
                mask_interior_label = labeled_mask[pos_y, pos_x]
                missing_labels = np.unique(spx_img[labeled_mask == mask_interior_label])
                for sp_label in missing_labels:
                    gth_img[(spx_img == sp_label).__and__(not_on_frontier)] = selection_on_g
                gth_lbl_list[-1].extend(missing_labels)
            gth_lbl_list.append([])
        else:
            for idx in range(gth_lbl_list.__len__()):
                if selected_sp_label in gth_lbl_list[idx]:
                    for region_label in gth_lbl_list[idx]:
                        gth_img[(spx_img == region_label).__and__(not_on_frontier)] = selection_off_g
                    del gth_lbl_list[idx]
                    gth_lbl_list.append([])
                    break

        tgt_canvas = self.tgt_canvas
        tgt_canvas.SetImage(gth_img)
        if tgt_canvas.magnifier_on:
            row_slice, col_slice = self.__MagnifierSlices__(pos_x, pos_y)
            tgt_canvas.magnifier.SetImage(gth_img[row_slice, col_slice])



    def mouseMoveEvent(self, event: qg_.QMouseEvent):
        #
        evt_x, evt_y = event.position().x(), event.position().y()
        pos_x, pos_y = self.__PixelCoordsFromEventXY__(evt_x, evt_y)

        self.tgt_pointer.move(int(evt_x) - circle_radius_g, int(evt_y) - circle_radius_g)
        self.parent_obj.info_zone.setText(f'row={pos_y}, col={pos_x} / x={pos_x}, y={self.img_height - pos_y}')

        row_slice = None
        col_slice = None

        if self.magnifier_on:
            if row_slice is None:
                row_slice, col_slice = self.__MagnifierSlices__(pos_x, pos_y)
            self.magnifier.SetImage(self.np_image[row_slice, col_slice])

        if self.tgt_canvas.magnifier_on:
            if row_slice is None:
                row_slice, col_slice = self.__MagnifierSlices__(pos_x, pos_y)
            self.tgt_magnifier.SetImage(self.tgt_mag_img[row_slice, col_slice])



    def ToggleMagnifier(self):
        #
        self.magnifier_on = not self.magnifier_on
        self.magnifier.setVisible(self.magnifier_on)



    def __PixelCoordsFromEventXY__(self, evt_x, evt_y):
        #
        pos_x = round(self.img_width  * evt_x / self.width())
        pos_y = round(self.img_height * evt_y / self.height())

        return pos_x, pos_y



    def __MagnifierSlices__(self, pos_x, pos_y):
        #
        row_slice = slice(max(pos_y - magnifier_radius_g, 0), min(pos_y + magnifier_radius_g, self.img_height))
        col_slice = slice(max(pos_x - magnifier_radius_g, 0), min(pos_x + magnifier_radius_g, self.img_width))

        return row_slice, col_slice



class circle_t(qw_.QWidget):

    def __init__(self, parent = None):
        #
        super().__init__(parent = parent)
        self.size = 2 * circle_radius_g + 1
        self.setGeometry(0, 0, self.size, self.size)
        self.setVisible(False)



    def paintEvent(self, _):
        #
        painter = qg_.QPainter(self)
        painter.setPen(qg_.QPen(qc_.Qt.GlobalColor.red))
        painter.drawEllipse(0, 0, self.size - 1, self.size - 1) # Dare I say the minus-ones are there because of a Qt bug?



def WatershedImage(observation):
    #
    filtered_img = fl_.gaussian(observation, channel_axis=2)
    grad_norm    = fl_.sobel(filtered_img)
    grad_norm   *= 255.0 / np.amax(grad_norm)
    local_minima = mp_.local_minima(filtered_img)
    local_minima = mp_.label(local_minima)

    return  sgmt.watershed(grad_norm, local_minima, watershed_line = False)



def InitialWatershedGroundTruth(wtd_image):
    #
    # import matplotlib.pyplot as plt
    # plt.matshow(wtd_image)
    # plt.matshow(ft_.canny(wtd_image).astype(np.float64))
    # plt.show()
    return selection_off_g * (1 - mp_.thin(fl_.sobel(wtd_image) > 0))



if __name__ == '__main__':
    #
    import sys as sy_

    app = qw_.QApplication(sy_.argv)
    main_wdw = main_wdw_t(sy_.argv)
    main_wdw.show()
    sy_.exit(app.exec())
